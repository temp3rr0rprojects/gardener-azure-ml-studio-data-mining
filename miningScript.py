# The script MUST contain a function named azureml_main
# which is the entry point for this module.

import requests
import pandas as pd
import numpy as np
from datetime import datetime

def azureml_main(dataframe1 = None, dataframe2 = None):

    print('Input pandas.DataFrame #1:\r\n\r\n{0}'.format(dataframe1))
    
    req = requests.get('https://api.darksky.net/forecast/<secret>/<secret>?units=si&exclude=currently,flags,daily,alerts')
    response = req.json()

    minDate = pd.to_datetime(response['hourly']['data'][0]['time'], unit='s')
    maxDate = pd.to_datetime(response['hourly']['data'][48]['time'], unit='s')
    dataFrameColumns = ['sensor.dark_sky_apparent_temperature',
                        'sensor.dark_sky_cloud_coverage', 'sensor.dark_sky_humidity', 'sensor.dark_sky_temperature',
                        'sensor.dark_sky_visibility', 'sensor.dark_sky_precip_intensity',
                        'sensor.dark_sky_precip_probability']
    ts = pd.DataFrame(np.nan, index=pd.date_range(start=minDate, end=maxDate, freq='Min'), columns=dataFrameColumns)
    
    for hourly_data in response['hourly']['data']:
        dataRow = []
        timeStamp = pd.to_datetime(hourly_data['time'], unit='s')
        ts.at[timeStamp, 'sensor.dark_sky_apparent_temperature'] = hourly_data['apparentTemperature']
        ts.at[timeStamp, 'sensor.dark_sky_cloud_coverage'] = hourly_data['cloudCover']
        ts.at[timeStamp, 'sensor.dark_sky_humidity'] = hourly_data['humidity']
        ts.at[timeStamp, 'sensor.dark_sky_temperature'] = hourly_data['temperature']
        ts.at[timeStamp, 'sensor.dark_sky_visibility'] = hourly_data['visibility']
        ts.at[timeStamp, 'sensor.dark_sky_precip_intensity'] = hourly_data['precipIntensity']
        ts.at[timeStamp, 'sensor.dark_sky_precip_probability'] = hourly_data['precipProbability']        
    
    ts = ts.ffill().bfill()
    ts.fillna(value=0.0, inplace=True)  # 0.0 if no value at all

    dataframe1 = ts

    return dataframe1,
