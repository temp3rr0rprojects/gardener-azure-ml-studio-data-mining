# Gardener Azure ML Studio Data Mining #

A python script for data mining of weather data (from Dark Sky API), used in Azure Machine Learning studio. Forward/backward fill interpolation is applied on missing data.

## Libraries
* pandas
* requests
* datetime